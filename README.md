https://teamx.hipchat.com/invite/190674/a7dee28869d0c9e26cde86866141b823

# AUFGABENSTELLUNG #
Im Rahmen des Praktikums soll jede Praktikumsgruppe eine Software 
entwickeln, die die folgenden Anforderungen erfüllt (ergänzende 
Anforderungen ggf. im Laufe des Projektes):

1. Erweiterung von Graph-Search, so dass die folgenden Anfragelogiken realisiert werden:
    1. Konjunktive und disjunktive Verknüpfung
    1. Komplementärmengen bzw. Negation
    1. Klammerausdrücke
    1. Gewichtung, Fuzzy-Suchen
1. Zwischenspeicherung und Filterung der Ergebnisse
    1. Bildabgleich (z.B. Gesichter, Flaggen, Logos)
    1. Verwendung von Schlagworthierarchien, Ontologien
1. Weiterverwendung der Ergebnisse als Eingabe für Suchwerkzeug
1. Visualisierung der Zwischen- und Endergebnisse (u.a. soll die 
„Herkunft“ der Ergebnisse jederzeit nachvollziehbar sein.
# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact