package data;

public class FBprofile {

	private int id;
	private String name;
	private int age;
	private String sex;
	private String from;
	private String lives;
	private String school_cur;
	private String school_old;
	private String worksAt;
	private String job;
	private String study_cur;
	private String study_old;
	private String university_cur;
	private String university_old;
	private String imageURL;
	
	public FBprofile(){
	}
	
	public FBprofile(int id, String name, int age, String sex, String from,
			String lives, String school_cur, String school_old, String worksAt,
			String job, String study_cur, String study_old,
			String university_cur, String university_old, String imageURL) {
		this.id = id;
		this.name = name.toLowerCase();
		this.age = age;
		this.sex = sex.toLowerCase();
		this.from = from.toLowerCase();
		this.lives = lives.toLowerCase();
		this.school_cur = school_cur.toLowerCase();
		this.school_old = school_old.toLowerCase();
		this.worksAt = worksAt.toLowerCase();
		this.job = job.toLowerCase();
		this.study_cur = study_cur.toLowerCase();
		this.study_old = study_old.toLowerCase();
		this.university_cur = university_cur.toLowerCase();
		this.university_old = university_old.toLowerCase();
		this.imageURL = imageURL;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name.toLowerCase();
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex.toLowerCase();
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from.toLowerCase();
	}

	public String getLives() {
		return lives;
	}

	public void setLives(String lives) {
		this.lives = lives.toLowerCase();
	}

	public String getSchool_cur() {
		return school_cur;
	}

	public void setSchool_cur(String school_cur) {
		this.school_cur = school_cur.toLowerCase();
	}

	public String getSchool_old() {
		return school_old;
	}

	public void setSchool_old(String school_old) {
		this.school_old = school_old.toLowerCase();
	}

	public String getWorksAt() {
		return worksAt;
	}

	public void setWorksAt(String worksAt) {
		this.worksAt = worksAt.toLowerCase();
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job.toLowerCase();
	}

	public String getStudy_cur() {
		return study_cur;
	}

	public void setStudy_cur(String study_cur) {
		this.study_cur = study_cur.toLowerCase();
	}

	public String getStudy_old() {
		return study_old;
	}

	public void setStudy_old(String study_old) {
		this.study_old = study_old.toLowerCase();
	}

	public String getUniversity_cur() {
		return university_cur;
	}

	public void setUniversity_cur(String university_cur) {
		this.university_cur = university_cur.toLowerCase();
	}

	public String getUniversity_old() {
		return university_old;
	}

	public void setUniversity_old(String university_old) {
		this.university_old = university_old.toLowerCase();
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	@Override
	public String toString() {
		return "FBprofile [id=" + id + ", name=" + name + ", age=" + age
				+ ", sex=" + sex + ", from=" + from + ", lives=" + lives
				+ ", school_cur=" + school_cur + ", school_old=" + school_old
				+ ", worksAt=" + worksAt + ", job=" + job + ", study_cur="
				+ study_cur + ", study_old=" + study_old + ", university_cur="
				+ university_cur + ", university_old=" + university_old
				+ ", imageURL=" + imageURL + "]";
	}
	
		
}
