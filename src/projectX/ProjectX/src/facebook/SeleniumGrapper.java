package facebook;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.FBprofile;

//import com.anteambulo.SeleniumJQuery.jQueryFactory;

public class SeleniumGrapper {
	public static List<FBprofile> profilesGrap(String query) {
		List<FBprofile> profiles = new ArrayList<>();
		
		// WebDriver driver = new PhantomJSDriver();
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("javascript.enabled", true);
		WebDriver driver = new FirefoxDriver(profile);

		driver.get("http://www.Facebook.com");
		driver.manage().deleteAllCookies();
		driver.get("http://www.Facebook.com");
		WebDriverWait wdw = new WebDriverWait(driver, 10);
		WebElement element = wdw.until(ExpectedConditions
				.visibilityOfElementLocated(By.id("email")));
		element.sendKeys("neo2k@gmx.net");
		element = wdw.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("pass")));
		element.sendKeys("15995000841122618");
		element.submit();
		element = wdw.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("_586i")));
		//element.sendKeys("Females who live in Hamburg University of Applied Sciences");
//		element.sendKeys("Females who study at Hamburg University of Applied Sciences and who are over 20 years old");
		element.sendKeys(query);
		//element.sendKeys("Females who live in Hamburg, Germany and study Computer Engineering");
		element.sendKeys(Keys.ENTER);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div[2]/div[2]/div/div[2]/div/div/div/div/div/ul/li[2]/a"))).click();

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		int personId = 0;
		String result1;
		String result2;
		
		 int scrollval = 1000;
		 for(int i=0 ; i < 50; i++) {//bis 50 zuvor
		  jse = (JavascriptExecutor)driver;
		 jse.executeScript(String.format("scroll(0, %d)",scrollval)); //y
//		 value '250' can be altered
		 try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		 scrollval += 1000;
		 }

		while(true) {
			
			String name = "";
			
			try {
				name = (String) jse.executeScript(String.format("return document.getElementsByClassName('_gll')[%d].firstChild.firstChild.firstChild.firstChild.innerHTML",personId));
			
			
			FBprofile fbprofile = new FBprofile();

			int idx = name.indexOf('<');
			if (!(idx < 0)) {
				name = name.substring(0, idx);
			}

			//System.out.println("Person nr.: " + personId);
			fbprofile.setId(personId);
			//System.out.println("Name: " + name);
			fbprofile.setName(name);

			String pic = (String) jse.executeScript(String.format("return document.getElementsByClassName('_glj')[%d].parentNode.parentNode.firstChild.firstChild.src",personId));
			//System.out.println("pic: " + pic);
			fbprofile.setImageURL(pic);
			
			String firstSubheader = (String) jse.executeScript(String.format("return document.getElementsByClassName('_glm')[%d].firstChild.innerHTML",personId));
			//System.out.println(firstSubheader);
			if(firstSubheader.indexOf("Works at <a href=") == 0) {
				result1 = firstSubheader.substring(firstSubheader.indexOf("\">") + 2, firstSubheader.indexOf("</a>"));
				//System.out.println("Works at: " + result1);
				fbprofile.setWorksAt(result1);
				
			} else if(firstSubheader.indexOf("a> at <a href=") > 0) {
				result1 = firstSubheader.substring(firstSubheader.indexOf("\">") + 2, firstSubheader.indexOf("</a> at <a href="));
				String sub = firstSubheader.substring(firstSubheader.indexOf("a> at <a href=") + 14);
				result2 = sub.substring(sub.indexOf("\">") + 2, sub.indexOf("</a>"));
				//System.out.println("Works: " + result1);
				fbprofile.setJob(result1);
				//System.out.println("At: " + result2);
				fbprofile.setWorksAt(result2);
			} 
			
			long count=(Long) jse.executeScript(String.format("return document.getElementsByClassName('_glo')[%d].firstChild.childNodes.length",personId));
			//System.out.println("Number of arguments: " + count);
			
			for (long j = 0; j < count; j++) {
				String attribute = (String) jse.executeScript(String.format("return document.getElementsByClassName('_glo')[%d].firstChild.childNodes[%d].firstChild.innerHTML",personId,j)); 
				//System.out.println("Attribute: " + attribute);
				
				if(attribute.indexOf("Went to ") == 0){
					result1 = attribute.substring(attribute.indexOf("\">") + 2, attribute.indexOf("</a>"));
					//System.out.println("Went to (School): " + result1);
					fbprofile.setSchool_old(result1);
					
				} else if(attribute.indexOf("Goes to ") == 0) {
					result1 = attribute.substring(attribute.indexOf("\">") + 2, attribute.indexOf("</a>"));
					//System.out.println("Goes to (School): " + result1);
					fbprofile.setSchool_cur(result1);
					
				} else if(attribute.indexOf("Studied at ") == 0) {
					result1 = attribute.substring(attribute.indexOf("\">") + 2, attribute.indexOf("</a>"));
					//System.out.println("Studied: " + result1);
					fbprofile.setUniversity_old(result1);
					
				} else if(attribute.indexOf("Studies at ") == 0) {
					result1 = attribute.substring(attribute.indexOf("\">") + 2, attribute.indexOf("</a>"));
					//System.out.println("Studies: " + result1);
					fbprofile.setUniversity_cur(result1);
					
				} else if(attribute.indexOf("Studied ") == 0) {
					result1 = attribute.substring("Studied ".length(), attribute.indexOf(" at <a href"));
					result2 = attribute.substring(attribute.indexOf("\">") + 2, attribute.indexOf("</a>"));
					//System.out.println("Studied: " + result1);
					fbprofile.setStudy_old(result1);
					//System.out.println("At: " + result2);
					fbprofile.setUniversity_old(result2);
					
				} else if(attribute.indexOf("Studies ") == 0) {
					result1 = attribute.substring("Studies ".length(), attribute.indexOf(" at <a href"));
					result2 = attribute.substring(attribute.indexOf("\">") + 2, attribute.indexOf("</a>"));
					//System.out.println("Studies: " + result1);
					fbprofile.setStudy_cur(result1);
					//System.out.println("At: " + result2);
					fbprofile.setUniversity_cur(result2);
					
				} else if(attribute.indexOf("Lives in <a href") == 0) {
					result1 = attribute.substring(attribute.indexOf("\">") + 2, attribute.indexOf("</a>"));
					//System.out.println("Lives is: " + result1);
					fbprofile.setLives(result1);
					attribute = attribute.substring(attribute.indexOf("</a>") + 2);
					
					if(attribute.indexOf("From <a href") >= 0) {
						result1 = attribute.substring(attribute.indexOf("\">") + 2, attribute.indexOf("</a>"));
						//System.out.println("And is from: " + result1);
						fbprofile.setFrom(result1);
					}
					
				} else if(attribute.indexOf("From <a href") >= 0) {
					result1 = attribute.substring(attribute.indexOf("\">") + 2, attribute.indexOf("</a>"));
					//System.out.println("Is from: " + result1);
					fbprofile.setFrom(result1);
					
				} else {
					if(attribute.indexOf("years old") >= 0) {
						int i = 1;
						int age = 0;
						int agePow = 1;
						int position = attribute.indexOf(" years old");
						
						while((position - i) >= 0 && attribute.charAt(position - i) >= '0' && attribute.charAt(position - i) <= '9') {
							age += ((int) attribute.charAt(position - i) - '0') * agePow;
							i++;
							agePow *= 10;
						}
						//System.out.println("Age: " + age);
						fbprofile.setAge(age);
					}
					if(attribute.indexOf("Female") >= 0) {
						//System.out.println("Is Girl");
						fbprofile.setSex("female");
					} else if(attribute.indexOf("Male") >= 0) {
						//System.out.println("Is Guy");
						fbprofile.setSex("male");
					}
				}
				
			}

			//System.out.println();
			profiles.add(fbprofile);
			personId++;
			} catch (Exception e) {

				// e.printStackTrace();
				//System.out.println("End of results.");
				break;
			}
		}

		driver.quit();
		return profiles;
	}
}
