package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import data.FBprofile;
import facebook.SeleniumGrapper;

public class MainFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	private JScrollPane scrollPane;
	private JPanel mainPanel;
	private JPanel querryPanel;
	private ResultPanel resultPanel;
	private JButton orButton;
	private JButton searchButton;
	private List<QuerryPanel> querryPanels;
	private List<String> queries;

	public MainFrame(){
		super("X-Search");
		
		scrollPane = new JScrollPane();
		scrollPane.setPreferredSize(new Dimension(1400,300));
		
		querryPanels = new ArrayList<QuerryPanel>();
		queries= new ArrayList<String>();

		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
	
		initQueryPanel();
		
		scrollPane.setViewportView(mainPanel);
		this.add(scrollPane);
		
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	@Override
	public Dimension getPreferredSize() { 
		return new Dimension(800,200); 
	}
	
	private void initQueryPanel(){
		querryPanel = new JPanel();
		querryPanel.setLayout(new BoxLayout(querryPanel,BoxLayout.Y_AXIS));
		
		QuerryPanel firstQuerry = new QuerryPanel();
		querryPanels.add(firstQuerry);
		querryPanel.add(firstQuerry);
		
		orButton = new JButton("or");
		orButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				onOrButton(e);
			}
		});
		querryPanel.add(orButton);
		
		searchButton = new JButton("search");
		searchButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				onSearch(e);
			}
		});
		mainPanel.add(querryPanel);
		mainPanel.add(searchButton);
		mainPanel.setPreferredSize(new Dimension(800,200));
	}
	
	private void onSearch(ActionEvent e){
		queries.clear();
		for(int i = 0; i < querryPanels.size();i++){
			String query = querryPanels.get(i).getQuery();
			System.out.println("Query: " + query);
			queries.add(query);
		}
		loadResultPanel();
	}
	
	private void onOrButton(ActionEvent e){
		QuerryPanel nextLinePanel = new QuerryPanel();
		querryPanel.add(nextLinePanel);	
		querryPanels.add(nextLinePanel);
		
		JButton tmpOrButton = new JButton("or");
		tmpOrButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				onOrButton(e);
			}
		});
		querryPanel.add(tmpOrButton);
		
		mainPanel.revalidate();
		mainPanel.repaint();
	}
	
	private void loadResultPanel(){
		List<ProfilesList> resultsList = getAllProfiles();
		if(!resultsList.isEmpty()){
			resultPanel = new ResultPanel(resultsList);
			resultPanel.newSearchButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e){
					onNewSearchButton(e);
				}
			});
			this.remove(scrollPane);
			this.add(resultPanel);
//			scrollPane.setViewportView(resultPanel);
//			scrollPane.revalidate();
//			scrollPane.repaint();
			this.revalidate();
			this.repaint();
		}
	}
	
	private List<ProfilesList> getAllProfiles(){
		List<ProfilesList> resultsList = new ArrayList<ProfilesList>();
		for(String q:queries){
			List<FBprofile> profiles = SeleniumGrapper.profilesGrap(q);
			if(!profiles.isEmpty()){
				ProfilesList result = new ProfilesList();
				result.setProfiles(profiles);
				result.setQuery(q);
				resultsList.add(result);
			}
		}
		return resultsList;
	}
	
	private void onNewSearchButton(ActionEvent e){
		this.remove(resultPanel);
		this.add(scrollPane);
		resetQueryPanel();
		queries.clear();
		this.revalidate();
		this.repaint();
	}
	
	private void resetQueryPanel(){
		querryPanels.clear();
		mainPanel.removeAll();
		querryPanel = null;
		
		initQueryPanel();
	}
	
	public static void main(String []args){
		new MainFrame();	
	}
}
