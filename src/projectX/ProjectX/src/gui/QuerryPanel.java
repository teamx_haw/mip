package gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class QuerryPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JButton deleteButton;
	private List<Component> components;
	private List<Integer> lastActiveIds;
	private int activeId;

	public QuerryPanel() {
		super();

		activeId = 0;
		components = new ArrayList<Component>();
		lastActiveIds = new ArrayList<Integer>();
		initPanel();

		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
	}
	
	public String getQuery(){
		String query = "";
		for(int i = 0; i < components.size(); i++){
			System.out.println("Compontes: " + components.get(i).getClass().getName() + "; Static: " + JComboBox.class + "size: " +  components.size());
			if(components.get(i).getClass() == JComboBox.class){
				JComboBox tmpCB = (JComboBox) components.get(i);
				query += " "+ tmpCB.getSelectedItem().toString();
			}else if(components.get(i).getClass() == JTextField.class){
				JTextField tmpTF = (JTextField) components.get(i);
				query += " "+ tmpTF.getText();
			}else if(components.get(i).getClass() == JLabel.class){
				JLabel tmpTF = (JLabel) components.get(i);
				query += " "+ tmpTF.getText();
			}
		}
		
		return query;
	}

	private void initPanel() {
		String[] valueArray = setValueOfCB(activeId);
		activeId = 0;

		JComboBox<String> typeCB = new JComboBox<String>(valueArray);
		typeCB.setSelectedIndex(0);
		typeCB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onChange(e);
			}
		});
		this.add(typeCB);
		components.add(typeCB);

		deleteButton = new JButton("Delete");
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onDeleteButton();
			}
		});
		this.add(deleteButton);
		components.add(deleteButton);
	}

	private void onChange(ActionEvent e) {
		JComboBox<String> cb = (JComboBox<String>) e.getSource();
		String value = (String) cb.getSelectedItem();
		System.out.println("ActiveId: " + activeId + "; value: " + value);
		if (value != "") {
			cb.setEnabled(false);
			if (activeId == 0) {
				activeId = 1;
				addComboBox(activeId);
			} else if (activeId == 1) {
				activeId = 2;
				addComboBox(activeId);
			} else if (activeId == 2 && value == "are") {
				activeId = 3;
				addComboBox(activeId);
			} else if (activeId == 2 && value == "study") {
				activeId = 4;
				addComboBox(activeId);
			} else if (activeId == 2 && value == "live") {
				activeId = 5;
				addComboBox(activeId);
			} else if (activeId == 2 && value == "like") {
				loadExtraTextField(false);
				activeId = 6;
				addComboBox(activeId);
			} else if (activeId == 3 && value == "single") {
				activeId = 6;
				addComboBox(activeId);
			} else if (activeId == 3 && value == "between") {
				loadExtraFieldsForBetween();
				activeId = 6;
				addComboBox(activeId);
			} else if (activeId == 4 && value == "nearby") {
				activeId = 6;
				addComboBox(activeId);
			} else if (activeId == 4 && value == "at") {
				loadExtraTextField(false);
				activeId = 6;
				addComboBox(activeId);
			} else if (activeId == 4 && value == "subject") {
				loadExtraTextField(true);
				activeId = 6;
				addComboBox(activeId);
			} else if (activeId == 5 && value == "nearby") {
				activeId = 6;
				addComboBox(activeId);
			} else if (activeId == 5 && value == "in") {
				loadExtraTextField(false);
				activeId = 6;
				addComboBox(activeId);
			} else if (activeId == 6) {
				activeId = 1;
				addComboBox(activeId);
			}
			lastActiveIds.add(activeId);
		}
	}

	private void onDeleteButton() {
		if (components.size() > 2) {
			int count = this.getComponentCount();
			if (count > 2) {
				if (lastActiveIds.size() >= 2) {
					lastActiveIds.remove(lastActiveIds.size() - 1);
					activeId = lastActiveIds.get(lastActiveIds.size() - 1);
					System.out.println("Active Id: " + activeId);
					while (activeId == 99) {
						deleteLastField();
						lastActiveIds.remove(lastActiveIds.size() - 1);
						activeId = lastActiveIds.get(lastActiveIds.size() - 1);
					}
				}else if(lastActiveIds.size() == 1){
					lastActiveIds.remove(lastActiveIds.size() - 1);
					activeId = 0;
				}

				count = this.getComponentCount();
				components.remove(count - 2);
				this.remove(count - 2);// das letzte Element ist immer der
										// Button zum Loschen

				System.out.println("size ArrayList Components: "
						+ components.size() + "; " + (components.size() - 2));
				System.out.println("size Components: " + getComponentCount()
						+ "; " + (getComponentCount() - 2));
				Component tmpComp = getComponent((getComponentCount() - 2));
				if (tmpComp.getClass() == JComboBox.class) {
					JComboBox tmpCB = (JComboBox) tmpComp;
					tmpCB.setEnabled(true);
				}

				this.revalidate();
				this.repaint();
			}
		}
	}

	private void deleteLastField() {
		int count = this.getComponentCount();
		components.remove(count - 2);
		this.remove(count - 2);
	}

	private void addComboBox(int id) {
		String[] valueArray = setValueOfCB(id);
		JComboBox<String> tmpCB = new JComboBox<String>(valueArray);
		tmpCB.setSelectedIndex(0);
		tmpCB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onChange(e);
			}
		});
		int index = components.size() - 1;
		components.add(index, tmpCB);

		this.remove(deleteButton);
		this.add(tmpCB);
		this.add(deleteButton);
		this.revalidate();
		this.repaint();
	}

	private String[] setValueOfCB(int tpye) {
		switch (tpye) {
		case 0: {
			String[] tmpArray = { "", "people", "friends", "male", "female", "musicians" };
			return tmpArray;
		}
		case 1: {
			String[] tmpArray = { "", "who" };
			return tmpArray;
		}
		case 2: {
			String[] tmpArray = { "", "are", "study", "live", "like" };
			return tmpArray;
		}
		case 3: {
			String[] tmpArray = { "", "single", "between" };
			return tmpArray;
		}
		case 4: {
			String[] tmpArray = { "", "subject", "at", "nearby" };
			return tmpArray;
		}
		case 5: {
			String[] tmpArray = { "", "in", "nearby" };
			return tmpArray;
		}
		case 6: {
			String[] tmpArray = { "", "and" };
			return tmpArray;
		}
		}
		return null;
	}

	private void loadExtraFieldsForBetween() {
		JTextField fromTF = new JTextField();
		fromTF.setPreferredSize(new Dimension(100,20));
		JLabel andLabel = new JLabel("and");
		JTextField tillTF = new JTextField();
		tillTF.setPreferredSize(new Dimension(100,20));

		int deleteButtonIndex = components.size() - 1;
		JButton deleteButton = (JButton) components.get(deleteButtonIndex);
		components.remove(deleteButtonIndex);
		this.remove(deleteButtonIndex);

		this.add(fromTF);
		this.add(andLabel);
		this.add(tillTF);
		this.add(deleteButton);

		components.add(fromTF);
		components.add(andLabel);
		components.add(tillTF);
		components.add(deleteButton);

		lastActiveIds.add(99);
		lastActiveIds.add(99);
		lastActiveIds.add(99);
	}

	private void loadExtraTextField(boolean deleteLastField) {
		JTextField extraTF = new JTextField();
		extraTF.setPreferredSize(new Dimension(100,20));
		
		if(deleteLastField){
			onDeleteButton();
			Component tmpComp = getComponent((getComponentCount() - 2));
			if (tmpComp.getClass() == JComboBox.class) {
				JComboBox tmpCB = (JComboBox) tmpComp;
				tmpCB.setEnabled(false);
			}
		}

		int deleteButtonIndex = components.size() - 1;
		JButton deleteButton = (JButton) components.get(deleteButtonIndex);
		components.remove(deleteButtonIndex);
		this.remove(deleteButtonIndex);

		this.add(extraTF);
		this.add(deleteButton);

		components.add(extraTF);
		components.add(deleteButton);

		lastActiveIds.add(99);
	}
}