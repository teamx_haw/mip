package gui;

import java.util.ArrayList;
import java.util.List;

import data.FBprofile;

public class ProfilesList {

	private List<FBprofile> profiles;
	private String query;
	
	public ProfilesList(){
		profiles = new ArrayList<FBprofile>();		
	}
	
	public ProfilesList(List<FBprofile> profiles, String query){
		this.profiles = profiles;
		this.query = query;
	}

	public List<FBprofile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<FBprofile> profiles) {
		this.profiles = profiles;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
}
