package gui;

import google.GoogleImageSearch;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import data.FBprofile;

public class ResultPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	private JScrollPane scrollPane; 
	private JPanel mainPanel;
	private JTextField imageSearchTF;
	private List<FBprofile> allResults;
	private List<FBprofile> imageSearchResults;
	
	public JButton newSearchButton;

	public ResultPanel(List<ProfilesList> listAllResult){
		super();
		allResults = new ArrayList<FBprofile>();
		imageSearchResults = new ArrayList<FBprofile>();
		scrollPane = new JScrollPane();
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(scrollPane);
		
		addControls();
		for(ProfilesList results: listAllResult)
			showResults(results);
	}
	@Override
	public Dimension getPreferredSize() { 
		return new Dimension(800,200); 
	}
	
	private void addControls(){
		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.X_AXIS));
		
		newSearchButton = new JButton("new search");
		controlPanel.add(newSearchButton);
		
		JButton imageSearchButton = new JButton("search in images: ");
		imageSearchButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				onImageSearch(e);
			}
		});
		controlPanel.add(imageSearchButton);
		
		imageSearchTF = new JTextField();
		controlPanel.add(imageSearchTF);
		this.add(controlPanel);
	}
	
	private void onImageSearch(ActionEvent e){
		String keywords = imageSearchTF.getText();
		System.out.println("KeyWords: " + keywords);
		GoogleImageSearch gis = new GoogleImageSearch();
		for(FBprofile profile: allResults){
			if(gis.findOnPicture(profile.getImageURL(), keywords)){
				System.out.println(profile);
				imageSearchResults.add(profile);
			}
		}
		gis.close();
		mainPanel.removeAll();
		
		JLabel queryLabel = new JLabel();
		queryLabel.setText("Image Search with: " + keywords);
		queryLabel.setFont(queryLabel.getFont().deriveFont(24));
		mainPanel.add(queryLabel);
		
		for(FBprofile result: imageSearchResults){
			addResultContainer(result);
		}
		this.revalidate();
		this.repaint();
	}
	
	private void showResults(ProfilesList results){
		JLabel queryLabel = new JLabel();
		queryLabel.setText("Query: " + results.getQuery());
		queryLabel.setFont(queryLabel.getFont().deriveFont(24));
		mainPanel.add(queryLabel);
		
		List<FBprofile> resultList = results.getProfiles();
		for(FBprofile result:resultList){
			addResultContainer(result);
			allResults.add(result);
		}
		scrollPane.setViewportView(mainPanel);
//		for(int i = 0; i < results.size(); i++){
//			FBprofile result = results.get(i);
//			addResultContainer(result);
//		}
	}
	
	private void addResultContainer(FBprofile result){
		JPanel resultPanel = new JPanel();
		resultPanel.setLayout(new BoxLayout(resultPanel, BoxLayout.X_AXIS));
		resultPanel.setPreferredSize(new Dimension(400,200));
		
		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
		
		try {
			URL url = new URL(result.getImageURL());
			BufferedImage image = ImageIO.read(url);
			ImagePanel imgPanel = new ImagePanel(image);
			resultPanel.add(imgPanel);
			
			setTextPanel(infoPanel,result);
			resultPanel.add(infoPanel);
			resultPanel.setBorder(BorderFactory.createLineBorder(Color.black));
			mainPanel.add(resultPanel);
			mainPanel.add(Box.createHorizontalStrut(50));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void setTextPanel(JPanel panel,FBprofile result){
		JLabel nameLabel = new JLabel(result.getName());
		panel.add(nameLabel);
		
		JLabel infoLabel = new JLabel();
		String info = "<html>";
		
		if(result.getAge() >  0){
			info += ("Age: " + result.getAge() + "<br>");
		}
		if(result.getFrom() != null){
			info += ("from: " + result.getFrom() + "<br>");
		}
		if(result.getLives() != null){
			info += ("lives: " + result.getLives() + "<br>");
		}
		if(result.getSchool_cur() != null){
			info += ("school_cur: " + result.getSchool_cur() + "<br>");
		}
		if(result.getSchool_old() != null){
			info += ("school_old: " + result.getSchool_old() + "<br>");
		}
		if(result.getWorksAt() != null){
			info += ("worksAt: " + result.getWorksAt() + "<br>");
		}
		if(result.getJob() != null){
			info += ("job: " + result.getJob() + "<br>");
		}
		if(result.getStudy_cur() != null){
			info += ("study_cur: " + result.getStudy_cur() + "<br>");
		}
		if(result.getStudy_old() != null){
			info += ("study_old: " + result.getStudy_old() + "<br>");
		}
		if(result.getUniversity_cur() != null){
			info += ("university_cur: " + result.getUniversity_cur() + "<br>");
		}
		if(result.getUniversity_old() != null){
			info += ("university_old: " + result.getUniversity_old() + "<br>");
		}
		
		info += "</html>";
		infoLabel.setText(info);
		panel.add(infoLabel);
	}
}
