package gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class ImagePanel extends JPanel{
	private static final long serialVersionUID = 1L;
	private static final int IMG_WIDTH = 200;
	private static final int IMG_HEIGHT = 200;
	private BufferedImage img;
	
	public ImagePanel(BufferedImage img){
		this.img = img;
	}
	@Override
	public Dimension getPreferredSize() { 
		return new Dimension(IMG_WIDTH,IMG_HEIGHT); 
	}
	@Override
	public void paintComponent(Graphics g) { 
		int x = 0; 
		int y = 0; 
		g.drawImage(img, x, y,IMG_WIDTH, IMG_HEIGHT,null); 
	}
}
