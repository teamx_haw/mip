<?php

/**
* Send a GET requst using cURL
* @param string $url to request
* @param array $get values to send
* @param array $options for cURL
* @return string
*/
function curl_get($url, array $get = NULL, array $options = array())
{   
    $defaults = array(
        CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get),
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_TIMEOUT => 4
    );
   
    $ch = curl_init();
    curl_setopt_array($ch, ($options + $defaults));
    if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    }
    curl_close($ch);
    return $result;
} 

// create a new cURL resource
//$ch = curl_init();
$pic = urlencode("http://i.imgur.com/j3shP.jpg");
$url = "https://images.google.com/searchbyimage?image_url=".$pic."&encoded_image=&image_content=&filename=";
echo $pic;
echo "<br />";
echo $url;

echo "<pre>";
var_dump(curl_get("http://images.google.com/searchbyimage", array("image_url"=>$pic),array()) );
echo "</pre>";

// set URL and other appropriate options
//curl_setopt($ch, CURLOPT_URL, $url);
//curl_setopt($ch, CURLOPT_HEADER, 0);

// grab URL and pass it to the browser
//curl_exec($ch);

// close cURL resource, and free up system resources
//curl_close($ch);

?>
