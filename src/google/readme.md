# Revers Image Search Plugin


####Request data:
```sh
{
   "SearchString":"String",
   "users":[
      {
         "fbId":"String",
         "urls":[
            {
               "url":"String"
            }
         ]
      }
   ]
}
```

####Response data:
```sh
{
   "filteredUsers":[
      {
         "fbId":"String"
      }
      "filteredUrls":[
            {
               "url":"String"
            }
   ]
}
```

