package imagesearch;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class ThesaurusGoogleKeyWordCloud {
	
	private GoogleImageSearch gis;
	private WebDriver driver;
	
	public ThesaurusGoogleKeyWordCloud(){
		gis = new GoogleImageSearch();
		FirefoxProfile profile = new FirefoxProfile();
	    profile.setPreference("javascript.enabled", true);
	    driver = new FirefoxDriver(profile);
	}
	
	public Set<String> findKeywords(String searchTerm){
		Set<String> words = new HashSet<String>();
		List<WebElement> meaningWords = new ArrayList<WebElement>();
		List<WebElement> lwe = new ArrayList<WebElement>();
		try{
			driver.get("http://synonyme.woxikon.de/synonyme-englisch/" + searchTerm.toLowerCase().replace(" ", "") + ".php");
			meaningWords = driver.findElements(By.className("meaningContent"));
			lwe = driver.findElements(By.className("inner"));
		} catch (Exception e) {
			e.printStackTrace();
		} 
		for(WebElement we: meaningWords){
			words.add(we.getText());
		}
		for(WebElement we_inner: lwe){
			List<WebElement> synonymWords = we_inner.findElements(By.tagName("a"));
			for(WebElement we_synonyme: synonymWords){
				words.add(we_synonyme.getText());
			}
		}		
		return words;
	}
	
	public List<String> findKeywordsOnPicture(String imageUrl, String searchTerm){		
		List<String> foundwords = new ArrayList<String>();
		foundwords = gis.findOnPicture(imageUrl, findKeywords(searchTerm));		
		return foundwords;
	}
	
	public List<String> findKeywordsOnPicture(String imageUrl, Set<String> words){
		List<String> foundwords = new ArrayList<String>();
		foundwords = gis.findOnPicture(imageUrl, words);
		return foundwords;
	}
	
	public void close(){
		gis.close();
		driver.quit();
	}
}
