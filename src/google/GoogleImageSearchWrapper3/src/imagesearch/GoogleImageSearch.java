package imagesearch;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class GoogleImageSearch {
	
	private WebDriver driver;
	
	public GoogleImageSearch() {
		FirefoxProfile profile = new FirefoxProfile();
	    profile.setPreference("javascript.enabled", true);
	    driver = new FirefoxDriver(profile);
	}
	
	public boolean findOnPicture(String imageUrl, String searchTerm) {
		boolean foundTerm = false;
		try {
			String requestURL = "https://images.google.com/searchbyimage?image_url=" + URLEncoder.encode(imageUrl,"utf8") + "&hl=en";
			driver.get(requestURL);
			String src = driver.getPageSource();
			src = src.replaceAll("<script>.*?</script>", "");
			//String src = driver.findElement(By.tagName("body")).getText();
			foundTerm = src.toLowerCase().contains(searchTerm.toLowerCase());
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return foundTerm;
	}
	
	public List<String> findOnPicture(String imageUrl, Set<String> searchTerms) {
		List<String> foundTerms = new ArrayList<String>();
		try {
			String requestURL = "https://images.google.com/searchbyimage?image_url=" + URLEncoder.encode(imageUrl,"utf8") + "&hl=en";
			driver.get(requestURL);
			String src = driver.getPageSource();
			src = src.replaceAll("<script>.*?</script>", "");
			//String src = driver.findElement(By.tagName("body")).getText();
			for(String s : searchTerms){
				if(src.toLowerCase().contains(s.toLowerCase())){
					foundTerms.add(s);
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return foundTerms;
	}
	
	public void close() {
		driver.quit();
	}
	
}
