﻿var node = '<ul><li><button class="remove">-</button> <select name="" id=""><option >und</option><option >oder</option><option >nicht</option><option >----------------</option><option selected="selected">Name</option><option >Wohnort</option><option >Herkunft</option><option >Geschlecht</option><option >...</option></select> <span class="field"><select name="" id=""><option>ist gleich</option><option>enthält</option><option>ähnelt</option></select> <input type="text" ></span></li><li class="new"><button class="add">+</button></li></ul>';

var child = '<li><button class="remove">-</button> <select name="" id=""><option >und</option><option >oder</option><option >nicht</option><option >----------------</option><option selected="selected">Name</option><option >Wohnort</option><option >Herkunft</option><option >Geschlecht</option><option >...</option></select> <span class="field"><select name="" id=""><option>ist gleich</option><option>enthält</option><option>ähnelt</option></select> <input type="text" ></span></li>'

$(document).ready(function(){
		initNode($('#filter'));
});


var initNode = function(object){
	object.find('select').unbind('change').change(function(){
		var select = $(this);
		switch($(this).val()) {
			case "und":
			case "oder":
				//
				select.siblings('.field').hide();
				if(!select.siblings('ul').length) initNode(select.parent().append(node));
				select.siblings('ul').children('.new').show();
				break;
			case "nicht":
				select.siblings('.field').hide();
				if(!select.siblings('ul').length) initNode(select.parent().append(node));
				select.siblings('.field').hide();
				select.siblings('ul').children('.new').hide();
				select.siblings('ul').children('li:not(.new, :first-child)').remove();
				break;
			default:
				select.siblings('.field').show();
				select.siblings('ul').remove();
				break;			
		}
	})
	object.find('button.add').unbind('click').click(function(){
		initNode($(child).insertBefore($(this).parent()));
	})
	object.find('button.remove').unbind('click').click(function(){
		$(this).parent().remove();
	})

	$("#search").submit(function (event) {
	    window.location.href = "results.html";
	    event.preventDefault();
	});
}