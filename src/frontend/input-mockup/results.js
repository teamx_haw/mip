$(document).ready(function () {
    showResults();
});

var showResults = function () {
    initTestData();

    for (var i = 0; i < resultData.length; i++) {
        var result = resultData[i];
        //$("#tableResults").append("<tr><td class='center'><input type='checkbox'></td><td>" + result.Id + "</td><td>Frank Test</td><td> 25</td><td>Deutschland</td><td>Stade</td></tr>");
        $(".results").append("<div class='result'><div class='profileImage'><img src='images/img1.jpg'></div><div class='infoText'><p>" 
            + result.Name + "</p><p>" + result.City + "," + result.Country + " </p></div></div>");
    }
    $("#sumResults").text("Total Results: " + resultData.length);
}

var resultData = [];

var initTestData = function () {
    for (var i = 0; i < 10; i++) {
        var result = new Object();
        result.Id = (i+1);
        result.Name = "Fank Test"
        result.Age = 25;
        result.Country = "Deutschland";
        result.City = "Stade";
        resultData[i] = result;
    }
}

function toggle(showHideDiv, switchTextDiv) {
    var ele = document.getElementById(showHideDiv);
    var text = document.getElementById(switchTextDiv);
    if (ele.style.display == "block") {
        ele.style.display = "none";
        text.innerHTML = "show";
    }
    else {
        ele.style.display = "block";
        text.innerHTML = "hide";
    }
}
