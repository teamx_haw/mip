var prepared = {
	_who: {
		'are': {
			'single': {
				'and': {
					'who': '_who'
				},
			},
			'between': {
				'_textfield' :{
					'and' : {
						'_textfield': {
							'and': {
								'who': '_who',
							},
						},
					}
				},
			},
		},
		'study': {
			'at': {
				'_textfield': {
					'and': {
						'who': '_who'
					},
				},
			},
			'nearby': {
				'and': {
					'who': '_who'
				},
			},
		},
		'live': {
			'in': {
				'_textfield': {
					'and': {
						'who': '_who'
					},
				},
			},
			'nearby': {
				'and': {
					'who': '_who'
				},
			},
		},
	}
}

var phrases = {
	'people': {
		'who': prepared._who
	},
	'friends': {
		'who': prepared._who
	},
	'females': {
		'who': prepared._who
	},
	'males' :{
		'who': prepared._who	
	}
}

var tree = {
	'root': phrases,
	'prepared': prepared	
}


$(document).ready(function(){
	$('#input').append(generate(phrases));	
	$('html').keyup(function(e){if(e.keyCode == 8) hremove();})
});
function generate(object, treePosition){
	var out="";
	if(typeof object === "object"){
		if(typeof treePosition !== "string") treePosition = "root";
		
		if(typeof object['_textfield'] !== "undefined"){
			out += "<input type='text' />";
			return [out,generate(object['_textfield'],treePosition + ",_textfield")];
		}
		else {
			out = "<select data-tree='" + treePosition.trim() + "'><option></option>";
			for(w in object){
				out += "<option>" + w + "</option>";
			}	
			out += "</select>";
			out = $(out);
			selectEvents(out);
		}	
	}
	else if(typeof object === "string" && object.substr(0,1) == "_"){
		out = generate(prepared[object], treePosition);
		selectEvents(out);		
	}

	return out;
}

function selectEvents(jO){
	jO.bind('change', function(){
		$(this).prevAll('select').prop( "disabled", true );
		var value = $(this).val();
		var treePosition = $(this).attr('data-tree');
		var nodes = [];
		if(treePosition) nodes = treePosition.trim().split(",");
		nodes.push(value);
		
		var p = tree;
		var treeNodes = nodes;
		for(i in nodes){
			n = nodes[i];
			p = p[n];
			if(typeof p === "string" && p.substr(0,1) == "_"){
				treeNodes = ['prepared',p];
			}
		}
		$(this).nextAll().remove();
		$('#input').append(generate(p,treeNodes.join()));
		

	});
}

function hremove(){
	if($('#input>select').length > 1){
		$('#input>*:last-child').remove();
		$('#input>input[type="text"]:last-child').remove();
		$('#input>select:last-child').prev().prop( "disabled", false );
		$('#input>select:last-child').prop( "disabled", false );
	}
}

function hsearch() {
	$('#query').text('');
	$('#input > *').each(function(){
		$('#query').append($(this).val() + ' ');
	});
}