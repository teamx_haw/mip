var searchFields = {
	'age_range': {
		label: 'Alter',
		fieldType: 'range'
	},
	'gender': {
		label: 'Geschlecht',
		fieldType: 'selection'
	},
	'hometown': {
		label: 'Herkunft',
		fieldType: 'string'
	},
	'first_name': {
		label: 'Vorname',
		fieldType: 'string'
	},
	'last_name': {
		label: 'Nachname',
		fieldType: 'string'
	},
	'relationship_status': {
		label: 'Beziehungsstatus',
		fieldType: 'selection'
	}/*,
	'': {
		label: '',
		fieldType: ''
	}*/
	
}

var fieldTypeOperators = {
	range: ['between'],
	string: ['equalText','like'],
	integer: ['equalText','less','greater'],
	selection: ['equalSelection']
}

var operators = {
	'equalText': {
		label: 'ist gleich',
		html: '<input type="text" class="equalText" />',
		query: {
			before: '',
			between: ' = "',
			behind: '"',
		}
	},
	'equalSelection': {
		label: 'ist gleich',
		html: '<select class="selection"><option>Lorem ipsum</option</select>',
		query: {
			before: '',
			between: ' = "',
			behind: '"',
		}
	},
	'less': {
		label: 'ist kleiner als',
		html: '<input type="text" class="less" />',
		query: {
			before: '',
			between: ' < ',
			behind: '',
		}
	},
	'greater': {
		label: 'ist größer als',
		html: '<input type="text" class="greater" />',
		query: {
			before: '',
			between: ' > ',
			behind: '',
		}
	},
	'contains': {
		label: 'beinhaltet',
		html: '<input type="text" class="contains" />',
		query: {
			before: '',
			between: ' CONTAINS "',
			behind: '"',
		}
	},
	'like': {
		label: 'ähnelt',
		html: '<input type="text" class="like" />',
		query: {
			before: '',
			between: ' LIKE "',
			behind: '"',
		}
	},
	'between': {
		label: 'zwischen',
		html: '<input type="text" class="between low" /> und <input type="text" class="between high" />',
		query: {
			before: '',
			between: ' BETWEEN ',
			behind: '',
		}
	}	
}