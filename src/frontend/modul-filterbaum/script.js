﻿$(document).ready(function(){
	initNode($('#filter').append(generateNodeHtml(true)));
});

var generateNodeHtml = function(isRoot) {
	var out;
	if(typeof isRoot === 'boolean' && isRoot){
		out = '<li><select name="" id="" class="conjunction"><option >und</option><option >oder</option><option >nicht</option><option >----------------</option>';	
	}
	else {
		out = '<li><button class="remove">-</button><select name="" id="" class="conjunction"><option >und</option><option >oder</option><option >nicht</option><option >----------------</option>';
	}
	var fieldOptions = '';
	var fieldHtml = '';
	var c = 0;
	for(var field in searchFields){
		if(c == 0) fieldOptions += '<option value="' + field + '" selected>' + searchFields[field].label + '</option>';
		else fieldOptions += '<option value="' + field + '">' + searchFields[field].label + '</option>';
		
		if(c == 0) fieldHtml += '<span class="field ' + field + '">';
		else fieldHtml += '<span class="field ' + field + '" style="display:none">';
		
		fieldHtml += '<select class="operator">';
		for(var operator in fieldTypeOperators[searchFields[field].fieldType]){
			fieldHtml += '<option value="'+ fieldTypeOperators[searchFields[field].fieldType][operator] +'">';
			fieldHtml += operators[fieldTypeOperators[searchFields[field].fieldType][operator]].label;
			fieldHtml += '</option>';
		}
		fieldHtml += '</select>';
		fieldHtml += '<input type="text" class="value" />';
		fieldHtml += '</span>';
		c++;
	}
	out += fieldOptions;
	out += '</select>';
	out += fieldHtml;
	return out;
}
var generateSublistHtml = function(){
	return '<ul>' + generateNodeHtml() + '<li class="new"><button class="add">+</button></li></ul>';
}

var initNode = function(object){
	object.find('select').unbind('change').change(function(){
		var select = $(this);
		switch($(this).val()) {
			case "und":
			case "oder":
				select.siblings('ul').children('li').children('button.remove').show();
				select.siblings('.field').hide();
				if(!select.siblings('ul').length) initNode(select.parent().append(generateSublistHtml()));
				select.siblings('ul').children('.new').show();
				break;
			case "nicht":
				select.siblings('.field').hide();
				if(!select.siblings('ul').length) initNode(select.parent().append(generateSublistHtml()));
				select.siblings('.field').hide();
				select.siblings('ul').children('.new').hide();
				select.siblings('ul').children('li:not(.new, :first-child)').remove();
				select.siblings('ul').children('li').children('button.remove').hide();
				break;
			default:
				select.siblings('ul').children('li').children('button.remove').show();
				select.siblings('.field').hide();
				select.siblings('.field.' + select.val()).show();
				select.siblings('ul').remove();
				break;			
		}
	})
	object.find('button.add').unbind('click').click(function(){
		initNode($(generateNodeHtml()).insertBefore($(this).parent()));
	})
	object.find('button.remove').unbind('click').click(function(){
		$(this).parent().remove();
	})
}

var printQuery = function(){
	$('#query').text("WHERE " + generateQuery($('#filter>li')));
}

var generateQuery = function(jQ){ // jQ entspricht dem jQ Objekt des Parent-<li>
	var out = "";
	var conj = jQ.children('select.conjunction').val();
	switch(conj) {
	case "und":
		out = "(true ";
		jQ.children('ul').children('li:not(.new)').each(function(){
			console.log( $(this));
			out += ' AND ' + generateQuery($(this));
		});
		out += ")";
		break;
	case "oder":
		out = "(false ";
		jQ.children('ul').children('li:not(.new)').each(function(){
			console.log( $(this));
			out += ' OR ' + generateQuery($(this));
		});
		out += ")";
		break;
		break;
	case "nicht":
		out = 'NOT(' + generateQuery(jQ.children('ul').children('li')) + ')';
		break;
	default:		
		var operator = operators[jQ.children('.field.' + conj).children('select.operator').val()];
		var value = jQ.children('.field.' + conj).children('input.value').val();
		out = operator.query.before + conj + operator.query.between + value + operator.query.behind;
		break;			
	}
	return out;
}