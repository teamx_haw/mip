<?php 
    require_once "DB.php";
    header("Content-Type: application/json"); // Datentyp auf JSON aendern

    if(isset($_POST["query"])){
        $query = $_POST["query"];

        $db = DB::instance();
        $result = $db->runQuery($query);

        echo json_encode($result);
    }
