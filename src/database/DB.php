<?php

require_once "DBconfig.php";

class DB {
    private $pdo,
            $stmt,
            $error = null;

    public function runQuery($query){
        $this->stmt = $this->pdo->query($query);
        return $this->stmt->fetchAll();
    }


    private function __construct()
    {
    	try{
            $dns = "mysql:host=" . Config::host . ";dbname=" . Config::dbname;
            $this->pdo = new PDO($dns, Config::username, Config::password, Config::$options);
        }catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /*
    *   CLASS SPECIFIC STUFF
    */
    private static $instance_ = null;

    public static function instance() {
        if (!isset(self::$instance_)) {
            self::$instance_ = new DB();
        }
        return self::$instance_;
    }
}
