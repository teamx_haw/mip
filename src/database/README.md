# DATABASE ABSCTRACTION LAYER #

## Table of Contens ##

* Version
* Usage

### Version ###
---------------
0.1 Basisimpelentierung

### Usage ###
-------------
* Um die DAL Klasse in JavaScript zu nutzen, ist es notwending die JavaScript-"Lib": DAL.js
> <script src="database/DAL.js"></script>

* Nach dem einbinden der Lib kann ein AJAX-Request an die Datenbank gesendet werden:
> DAL.requestQuery(query)

* als Antwort erfolgt ein Array mit JSON ObjeKten.
