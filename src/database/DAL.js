/*
*  DATABASE ABSTRACTION LAYER
*/

var DAL = {
    requestQuery : function(query){
      var xmlhttp;
      var answer = 0;
      if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp = new XMLHttpRequest();
        }else{// code for IE6, IE5
          xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
      xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
            //document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
            answer = xmlhttp.responseText;
          }
        }
      xmlhttp.open("POST", "DBaccess.php", false);
      xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
      xmlhttp.send("query=" + query);
      return JSON.parse(answer);
    }
}