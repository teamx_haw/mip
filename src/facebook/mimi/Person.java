package mimi;

public class Person {
	private String name;
	private String gender;
	private String relationship;
	private String birthplace;
	private String livesIn;
	public Person(String name, String gender, String relationship,
			String birthplace, String livesIn, String cocksize) {
		super();
		this.name = name;
		this.gender = gender;
		this.relationship = relationship;
		this.birthplace = birthplace;
		this.livesIn = livesIn;
		this.cocksize = cocksize;
	}
	private String cocksize;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getBirthplace() {
		return birthplace;
	}
	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((birthplace == null) ? 0 : birthplace.hashCode());
		result = prime * result
				+ ((cocksize == null) ? 0 : cocksize.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((livesIn == null) ? 0 : livesIn.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((relationship == null) ? 0 : relationship.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (birthplace == null) {
			if (other.birthplace != null)
				return false;
		} else if (!birthplace.equals(other.birthplace))
			return false;
		if (cocksize == null) {
			if (other.cocksize != null)
				return false;
		} else if (!cocksize.equals(other.cocksize))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (livesIn == null) {
			if (other.livesIn != null)
				return false;
		} else if (!livesIn.equals(other.livesIn))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (relationship == null) {
			if (other.relationship != null)
				return false;
		} else if (!relationship.equals(other.relationship))
			return false;
		return true;
	}
	public String getLivesIn() {
		return livesIn;
	}
	public void setLivesIn(String livesIn) {
		this.livesIn = livesIn;
	}
	public String getCocksize() {
		return cocksize;
	}
	public void setCocksize(String cocksize) {
		this.cocksize = cocksize;
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", gender=" + gender
				+ ", relationship=" + relationship + ", birthplace="
				+ birthplace + ", livesIn=" + livesIn + ", cocksize="
				+ cocksize + "]";
	}
	
}
